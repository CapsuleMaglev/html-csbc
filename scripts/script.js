﻿/***************/
/*** FUNÇÕES ***/
/***************/

// Função resetapagina(numberofpages, pagenum)
// Objetivo: reseta a página de uma janela para a página 1
// Entradas:
//		numberofpages: número total de páginas atualmente
//		pagenum: página atual
function resetapagina(numberofpages, pagenum) {
	newIcon = "#page1"
	oldIcon = "#page".concat(pagenum);
	$(oldIcon.concat(".".concat(numofpages))).attr('src', 'css/page.png');
	$(newIcon.concat(".".concat(numofpages))).attr('src', 'css/sel_page.png');
}

// Função cycleImages()
// Objetivo: mudar o plano de fundo do website de intervalo em intervalo, usando fadeIn/fadeOut.
// Algoritmo vindo da internet 
function cycleImages(){
      var $active = $('#background_cycler .active');
      var $next = ($('#background_cycler .active').next().length > 0) ? $('#background_cycler .active').next() : $('#background_cycler img:first');
      $next.css('z-index',2);//move the next image up the pile
	  $active.fadeOut(1500,function(){//fade out the top image
	  $active.css('z-index',1).show().removeClass('active');//reset the z-index and unhide the image
      $next.css('z-index',3).addClass('active');//make the next image the top one
      });
    }

    $(window).load(function(){
		$('#background_cycler').fadeIn(1500);//fade the background back in once all the images are loaded
		  // run every 7s
		  setInterval('cycleImages()', 7000);
    })
 
/***************/
/*** EVENTOS ***/
/***************/

// Variáveis
var pageno = 1;		// número da página atual, usado para navegar entre as páginas de cada PET
var onprocess = 0;	// variál usada na navegação das páginas. Veja os eventos de "pageright" e "pageleft" para mais detalhes
var current = '#Brasilzao';	// variál que armazena a janela atual mostrada no bloco principal
var numofpages = 1;	// variável que armazena quantas páginas uma janela tem

var pageSelectorSize;	// variável usada para mudar os ícones ao fundo da janela, quando se troca a página
var newIcon;			// variável usada para mudar os ícones ao fundo da janela, quando se troca a página
var oldIcon;			// variável usada para mudar os ícones ao fundo da janela, quando se troca a página

/*** EVENTOS DAS JANELAS - INÍCIO ***/
/* BOTÃO PARA RETORNAR AO MAPA */                          
$('#miniBrasil').click(function() { $('#pageSelector').slideUp("fast");
							setTimeout(function() {
								$('#miniBrasil').slideUp("fast");
								$(current).slideUp("fast");
								$('#pageSelector.'.concat(numofpages)).fadeOut(1);
								},200);
                            setTimeout(function() {
								current = '#Brasilzao';
								$(current).slideDown(700);
								pageno = 1;
								numofpages = 1;
								},400);
							resetapagina(numofpages, pageno);
							});							
							
/* MANIPULAÇÃO DE PÁGINAS */
// Anda uma página para esquerda						
$('#pageleft').click(function() {
	if (onprocess == 0) {		// evita que o usuário clique muito rapidamente no botão e ocorram bugs na página
		onprocess = 1;
		if (pageno > 1) {
			pageno--;
			$(current).fadeOut(300);
			setTimeout(function() {
				current = (current.substr(0, current.length - 1)).concat(pageno);
				$(current).fadeIn(300);
				}, 300);
				
			newIcon = "#page".concat(pageno);
			oldIcon = "#page".concat(pageno+1);
			$(oldIcon.concat(".".concat(numofpages))).attr('src', 'css/page.png');
			$(newIcon.concat(".".concat(numofpages))).attr('src', 'css/sel_page.png');
		}
		
		setTimeout(function() { onprocess = 0 }, 601);
	}
});

$('#pageleft').mouseover(function() {
	$('#pageleft').attr('src', 'css/pageleft_mouseover.png');
});

$('#pageleft').mouseleave(function() {
	$('#pageleft').attr('src', 'css/pageleft.png');
});

// Anda uma página para direita 
$('#pageright').click(function() {
	if (onprocess == 0) {
		onprocess = 1;
		if (pageno < numofpages) {
			pageno++;
			$(current).fadeOut(300);
			setTimeout(function() {
				current = (current.substr(0, current.length - 1)).concat(pageno);
				$(current).fadeIn(300);
				}, 300);

			newIcon = "#page".concat(pageno);
			oldIcon = "#page".concat(pageno-1);
			$(oldIcon.concat(".".concat(numofpages))).attr('src', 'css/page.png');
			$(newIcon.concat(".".concat(numofpages))).attr('src', 'css/sel_page.png');
		}
		
		setTimeout(function() { onprocess = 0 }, 601);
	}
}); 

$('#pageright').mouseover(function() {
	$('#pageright').attr('src', 'css/pageright_mouseover.png');
});

$('#pageright').mouseleave(function() {
	$('#pageright').attr('src', 'css/pageright.png');
});						

/* Etc */			

/*** FIM DOS EVENTOS DAS JANELAS ***/

/*** EVENTOS DO MAPA - INÍCIO ***/
$("#RS").on("click",function(){ 
				$("#imagem").css(
					{
					"height": "100px",
					"width": "100px"}); 
				$("#pteste").text( "HOLY EASTER EGG!" );	
				} )

				
$('#PR').click(function() { $('.side').show("slow","swing"); });
$('#SC').click(function() { $('.side').hide("slow","linear"); });

$('#BA').click(function() {$('.PR').show("fast","swing");
                           document.getElementById("videoPR").load();
                           document.getElementById("videoPR").play()   });


$('#MT').click(function() { $('.PR').hide("slow","linear");
                            document.getElementById("videoPR").pause(); });

$("#MG").click(function() {$('#oquee').animate({width:"200px"},400)});

$('#PA').click(function() {
                            $('#Brasilzao').slideUp();
                            setTimeout(function() {$('#miniBrasil').slideDown(400)},700);
                            setTimeout(function() {$('#petInfo.list_PA').slideDown(400)},700);
							setTimeout(function() {$('#pageSelector').slideDown(400)},1100);
                            document.getElementById("videoPR").pause(); 
							pageno = 1
							current = '#petInfo.list_PA';
							});
							
$('#SP').click(function() {
                            $('#Brasilzao').slideUp();
                            setTimeout(function() {
								current = '#petInfo.list_SP';
								$('#miniBrasil').slideDown(400);
								$(current).slideDown(400);
								},700);
							pageno = 1;
							});							
/*** FIM DOS EVENTOS DO MAPA ***/	

/*** EVENTOS DAS JANELAS DE CADA ESTADO ***/

//São Paulo

$('#pMainLink.SP_SI_USP').click(function() {
							$(current).fadeOut(300);
                            setTimeout(function() {
								current = '#petInfo.SP_USP_SI-P1';
								$(current).fadeIn(300);
								$('#pageSelector.'.concat(numofpages)).fadeIn(300);
								},300);
							pageno = 1;
							numofpages = 2;
							});				

/*** FIM DOS EVENTOS DAS JANELAS DE CADA ESTADO ***/	
	
							
					